﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace InteractiveGraph
{
	public partial class InteractiveGraphControl : Control
	{

		private void OnMouseButtonDown(MouseButtonEventArgs e)
		{
			CaptureMouse();
			switch(_tool)
			{
				case Tool.Cursor:
					CursorDown(e);
					break;
				case Tool.Pencil:
					PencilDown(e);
					break;
				case Tool.Hand:
					HandDown(e);
					break;
			}
		}

		private void HandDown(MouseButtonEventArgs e)
		{
			if(e.LeftButton != MouseButtonState.Pressed)
				return;
			_lastPoint = new PointAdapter(e.GetPosition(this));
		}

		private void PencilDown(MouseButtonEventArgs e)
		{
			if(e.LeftButton != MouseButtonState.Pressed)
				return;
			_model.Points.Add(_converter.ToWorld(new PointAdapter(e.GetPosition(this))));
		}

		private void CursorDown(MouseButtonEventArgs e)
		{
			IPoint<double> aimedPoint = _model.HoverPoints.FirstOrDefault(x => IsMouseOnPoint(e, x));
			if(_model.SelectedPoints.Any(x => IsMouseOnPoint(e, x)))
			{
				_mover = new MouseMover(_model, _converter.ToWorld(new PointAdapter(e.GetPosition(this))));
			}
			else if(aimedPoint != null)
			{
				RefreshPoints();
				_model.Points.Remove(aimedPoint);
				_model.SelectedPoints.Add(aimedPoint);
				_mover = new MouseMover(_model, _converter.ToWorld(new PointAdapter(e.GetPosition(this))));
			}
			else
			{
				RefreshPoints();
				_selector = new Selector(_converter.ToWorld(new PointAdapter(e.GetPosition(this))), _model);
			}
		}

		private void OnMouseButtonUp(MouseButtonEventArgs e)
		{
			switch(_tool)
			{
				case Tool.Cursor:
					CursorUp(e);
					break;
				case Tool.ZoomIn:
					ZoomInUp(e);
					break;
				case Tool.ZoomOut:
					ZoomOutUp(e);
					break;
				case Tool.Hand:
					HandUp(e);
					break;
			}
			Mouse.Capture(null);
		}

		private void HandUp(MouseButtonEventArgs e)
		{
			if(e.ChangedButton != MouseButton.Left)
				return;
			_lastPoint = null;
		}

		private void ZoomInUp(MouseButtonEventArgs e)
		{
			if(e.ClickCount == 1 && e.ChangedButton == MouseButton.Left && ScaleFactor > 0.01)
				ScaleFactor /= 2;
		}

		private void ZoomOutUp(MouseButtonEventArgs e)
		{
			if(e.ClickCount == 1 && e.ChangedButton == MouseButton.Left && ScaleFactor < 8)
				ScaleFactor *= 2;
		}

		private void CursorUp(MouseButtonEventArgs e)
		{
			_selector?.Select();
			_selector = null;
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonDown(e);
			OnMouseButtonDown(e);
		}
		protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonUp(e);
			OnMouseButtonUp(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			switch(_tool)
			{
				case Tool.Cursor:
					CursorMove(e);
					break;
				case Tool.Pencil:
					PencilMove(e);
					break;
				case Tool.Hand:
					HandMove(e);
					break;
			}
		}

		private void HandMove(MouseEventArgs e)
		{
			if(_lastPoint == null || e.LeftButton != MouseButtonState.Pressed)
				return;
			IPoint<double> mousePos = new PointAdapter(e.GetPosition(this));
			double diff = mousePos.X - _lastPoint.X;
			ScaleDisplacement += diff;
			_lastPoint = mousePos;
		}

		private void PencilMove(MouseEventArgs e)
		{
			if(e.LeftButton != MouseButtonState.Pressed)
				return;
			_model.Points.Add(_converter.ToWorld(new PointAdapter(e.GetPosition(this))));
		}

		private void CursorMove(MouseEventArgs e)
		{
			UpdateSelector(e.GetPosition(this));
			if(_selector != null)
				return;
			try
			{
				var aimedPoint = _model.Points.First(x => IsMouseOnPoint(e, x));
				_model.Points.Remove(aimedPoint);
				_model.HoverPoints.Add(aimedPoint);
			}
			catch(InvalidOperationException)
			{
			}
			try
			{
				var notAimedPoint = _model.HoverPoints.First(x => !IsMouseOnPoint(e, x));
				_model.HoverPoints.Remove(notAimedPoint);
				_model.Points.Add(notAimedPoint);
			}
			catch(InvalidOperationException)
			{
			}
			if(_mover is MouseMover)
			{
				_mover.Move(_converter.ToWorld(new PointAdapter(e.GetPosition(this))));
			}
		}

		protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseRightButtonDown(e);
			OnMouseButtonDown(e);
		}

		protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
		{
			base.OnMouseRightButtonUp(e);
			OnMouseButtonUp(e);
		}
	}
}
