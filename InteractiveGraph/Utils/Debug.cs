﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveGraph.Utils
{
	static class Debug
	{
		static TextWriter output = new StreamWriter("debug.out");

		static public void Message(params object[] args)
		{
			foreach(var arg in args)
				output.Write(arg + " ");
			output.WriteLine();
			output.Flush();
		}
	}
}
