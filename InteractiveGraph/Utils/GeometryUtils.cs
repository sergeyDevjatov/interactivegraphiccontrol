﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveGraph.Utils
{
	static class GeometryUtils
	{
		static bool IsMouseOnPoint(IPoint<double> mousePos, IPoint<double> checkPoint, double radius)
		{
			double deltaX = checkPoint.X - mousePos.X;
			double deltaY = checkPoint.Y - mousePos.Y;
			return (Math.Pow(deltaX, 2) + Math.Pow(deltaY, 2)) < (Math.Pow(radius, 2));
		}
	}
}
