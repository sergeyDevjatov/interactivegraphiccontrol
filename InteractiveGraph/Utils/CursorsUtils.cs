﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace InteractiveGraph.Utils
{
	static class CursorsUtils
	{
		static Cursor _zoomInCursor = new Cursor(new System.IO.FileStream("zoom_in.cur", System.IO.FileMode.Open));
		static Cursor _zoomOutCursor = new Cursor(new System.IO.FileStream("zoom_out.cur", System.IO.FileMode.Open));
		static public Cursor ZoomIn { get => _zoomInCursor; }
		static public Cursor ZoomOut { get => _zoomOutCursor; }
	}
}
