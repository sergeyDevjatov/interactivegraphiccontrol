﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InteractiveGraph
{
	class Zoomer
	{
		private IPoint<double> _from;
		private IPoint<double> _to;
		private double _range;
		private double _scaleFactor = DefaultScaleFactor;

		public Zoomer(IPoint<double> p, double range, double scaleFactor)
		{
			From = p;
			To = From;
			_range = range;
			_scaleFactor = scaleFactor;
		}

		public double Diff
		{
			get
			{
				return To.X - From.X;
			}
		}

		public IPoint<double> From
		{
			get => _from;
			set
			{
				_from = value;
				ScaleFactorChanged?.Invoke(this, new EventArgs());
			}
		}
		public IPoint<double> To
		{
			get => _to;
			set
			{
				_from = _to;
				_to = value;
				ScaleFactorChanged?.Invoke(this, new EventArgs());
			}
		}
		public event EventHandler ScaleFactorChanged;
		public double ScaleFactor { get { Utils.Debug.Message(Diff, _range); _scaleFactor -=  Diff / (_range); return _scaleFactor; } }
		static public readonly double DefaultScaleFactor = 1; 
	}
}
