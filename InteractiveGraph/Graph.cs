﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace InteractiveGraph
{
	public class Graph : IEnumerable<Point>
	{
		public LinkedList<Point> Points { get; set; } = new LinkedList<Point>();

		public IEnumerator<Point> GetEnumerator()
		{
			return ((IEnumerable<Point>)Points).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<Point>)Points).GetEnumerator();
		}
	}
}
