﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveGraph
{
	class MouseMover : IMover
	{
		private IGraphViewModel _model;
		private IPoint<double> _from;

		static private IPoint<double> Move(IPoint<double> point, IPoint<double> displacement) =>
			new PointAdapter(point.X + displacement.X, point.Y + displacement.Y);

		public void Move(IPoint<double> to)
		{
			_model.SelectedPoints = new ObservableCollection<IPoint<double>>(_model.SelectedPoints.Select(x => Move(x, new PointAdapter(to.X - _from.X, to.Y - _from.Y))));
			_from = to;
		}

		public MouseMover(IGraphViewModel model, IPoint<double> from)
		{
			_model = model;
			_from = from;
		}

	}
}
