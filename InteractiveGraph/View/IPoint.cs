﻿namespace InteractiveGraph
{
	interface IPoint<T>
	{
		T X { get; set; }
		T Y { get; set; }


		System.Windows.Point ToPoint();
	}
}
