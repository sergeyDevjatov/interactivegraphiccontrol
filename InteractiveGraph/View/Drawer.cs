﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace InteractiveGraph.View
{
	class Drawer : IDrawer
	{
		DrawingContext _context;
		ICoordinateConverter<double> _converter;

		private byte Code(IPoint<double> p)
		{
			byte code = (byte)((Convert.ToByte(p.X < _converter.Screen.Left) << 3) |
			(Convert.ToByte(p.X > _converter.Screen.Right) << 2) |
			(Convert.ToByte(p.Y < _converter.Screen.Top) << 1) |
			Convert.ToByte(p.Y > _converter.Screen.Bottom));
			return code;
		}

		public Drawer(DrawingContext context, ICoordinateConverter<double> converter)
		{
			_context = context;
			_converter = converter;
		}

		public void DrawPoints(IEnumerable<IPoint<double>> points, Brush pointBrush, double radius)
		{
			if(CantToDraw() || PointsAreNotExist(points))
				return;
			foreach(var g in points.Where(p => p.X >= _converter.World.Left && p.X <= _converter.World.Right))
			{
				IPoint<double> screenPoint = _converter.ToScreen(g);
				_context.DrawEllipse(pointBrush, null, screenPoint.ToPoint(), radius, radius);
			}
		}

		private static bool PointsAreNotExist(IEnumerable<IPoint<double>> points)
		{
			return points == null || points.Count() <= 0;
		}

		private bool CantToDraw()
		{
			return _context == null || _converter == null;
		}

		public void DrawSelector(ISelector selector, Brush background, Brush border)
		{
			if(CantToDraw() || selector == null)
				return;
			Brush backgroundWithOpacity = background.Clone();
			backgroundWithOpacity.Opacity = 0.25;
			Point from = _converter.ToScreen(selector.From).ToPoint(),
				  to = _converter.ToScreen(selector.To).ToPoint();
			from.X += 0.5;
			from.Y += 0.5;
			to.X += 0.5;
			to.Y += 0.5;
			_context.DrawRectangle(backgroundWithOpacity,
								   new Pen(border, 0.5),
								   new Rect(from, to));
		}

		private void DrawLine(Pen pen, IPoint<double> a, IPoint<double> b)
		{
			if(a == b || Math.Abs(a.X - b.X) < 0.5 && Math.Abs(a.Y - b.Y) < 0.5)
				return;
			if((Code(a) | Code(b)) == 0)
			{
				_context.DrawLine(pen, a.ToPoint(), b.ToPoint());
				return;
			}
			if((Code(a) & Code(b)) != 0)
				return;

			IPoint<double> mid = new PointAdapter((a.X + b.X) / 2,
												  (a.Y + b.Y) / 2);
			DrawLine(pen, a, mid);
			DrawLine(pen, mid, b);
		}

		public void DrawLines(IEnumerable<IPoint<double>> points, Brush lineBrush, double thickness)
		{
			if(CantToDraw() || points == null || points.Count() < 2)
				return;
			Pen pen = new Pen(lineBrush, thickness);
			IEnumerable<IPoint<double>> orderedPoints = points.OrderBy(p => p.X).ThenBy(p => p.Y).Select(_converter.ToScreen);
			IPoint<double> prev = orderedPoints.First();
			foreach(var p in orderedPoints.Skip(1))
			{
				DrawLine(pen, prev, p);
				prev = p;
			}
		}

		public void DrawScale(IEnumerable<IPoint<double>> points, Brush lineBrush, double lineThickness)
		{
			if(CantToDraw() || points == null || points.Count() < 2)
				return;
			double from = Math.Ceiling(_converter.World.Bottom), to = _converter.World.Top;
			var graduation = MakeGraduation(points.Max(p => p.Y) - points.Min(p => p.Y));
			Pen linePen = new Pen(lineBrush, lineThickness)
			{
				DashStyle = DashStyles.Dash
			};
			
			while(from % graduation.pointValue != 0) from += 1;

			while(to < from - graduation.pointValue)
			{
				from -= graduation.pointValue;
				_context.DrawLine(linePen, _converter.ToScreen(new PointAdapter(_converter.World.Left, from)).ToPoint(), _converter.ToScreen(new PointAdapter(_converter.World.Right, from)).ToPoint());
				var text = new FormattedText($"{from}", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Times New Roman"), 12, Brushes.Black);
				_context.DrawText(text, _converter.ToScreen(new PointAdapter(_converter.World.Left, from)).ToPoint());
			}

			from = Math.Ceiling(_converter.World.Right);
			to = _converter.World.Left;
			while(from % graduation.pointValue != 0) from += 1;
			while(to < from - graduation.pointValue)
			{
				from -= graduation.pointValue;
				_context.DrawLine(linePen, _converter.ToScreen(new PointAdapter(from, _converter.World.Top)).ToPoint(), _converter.ToScreen(new PointAdapter(from, _converter.World.Bottom)).ToPoint());
				var text = new FormattedText($"{from}", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Times New Roman"), 12, Brushes.Black);
				_context.DrawText(text, _converter.ToScreen(new PointAdapter(from, _converter.World.Top)).ToPoint());
			}
		}

		struct Graduation { public int pointsCount; public int pointValue; }
		private Graduation MakeGraduation(double num)
		{
			if(num < 10)
				return new Graduation() { pointsCount = 5, pointValue = 2 };

			int[] potentialStripsCounts = new int[] { 5, 6, 7 };
			num = (float)Math.Round(num);
			int capacity = (int)Math.Log10(num);
			int del = (int)Math.Pow(10, capacity);
			int nearestRound = (int)Math.Ceiling(num / del) * del;
			int i = 0;
			for(; i < potentialStripsCounts.Length; ++i)
				if(nearestRound % potentialStripsCounts[i] == 0)
					break;

			return new Graduation()
			{
				pointsCount = potentialStripsCounts[i],
				pointValue = nearestRound / potentialStripsCounts[i]
			};
		}
	}
}
