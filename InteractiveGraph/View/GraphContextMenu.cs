﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace InteractiveGraph.View
{
	class GraphContextMenu : ContextMenu
	{
		public MenuItem DeleteItem { get; }
		public MenuItem ToolsItem { get; }
		public MenuItem CursorToolItem { get; }
		public MenuItem HandToolItem { get; }
		public MenuItem ZoomInToolItem { get; }
		public MenuItem ZoomOutToolItem { get; }
		public MenuItem PencilToolItem { get; }

		public GraphContextMenu()
		{
			DeleteItem = new MenuItem()
			{
				Header = "Удалить"
			};
			DeleteItem.Click += OnDeleteItemClick;

			Items.Add(DeleteItem);
			Items.Add(new Separator());

			ToolsItem = new MenuItem()
			{
				Header = "Инструменты"
			};

			CursorToolItem = new MenuItem()
			{
				Header = "Курсор",
				
			};
			CursorToolItem.Click += OnCursorToolItemClick;

			MenuItem HandToolItem = new MenuItem()
			{
				Header = "Рука"
			};
			HandToolItem.Click += OnHandToolItemClick;

			ZoomInToolItem = new MenuItem()
			{
				Header = "Увеличить"
			};
			ZoomInToolItem.Click += OnZoomInToolItemClick;

			ZoomOutToolItem = new MenuItem()
			{
				Header = "Уменьшить"
			};
			ZoomOutToolItem.Click += OnZoomOutToolItemClick;

			PencilToolItem = new MenuItem()
			{
				Header = "Карандаш"
			};
			PencilToolItem.Click += OnPencilToolItemClick;

			ToolsItem.Items.Add(CursorToolItem);
			ToolsItem.Items.Add(HandToolItem);
			ToolsItem.Items.Add(ZoomInToolItem);
			ToolsItem.Items.Add(ZoomOutToolItem);
			ToolsItem.Items.Add(PencilToolItem);
			Items.Add(ToolsItem);

		}

		public event RoutedEventHandler ZoomOutToolItemClicked;
		private void OnZoomOutToolItemClick(object sender, RoutedEventArgs e)
		{
			ZoomOutToolItemClicked(sender, e);
		}

		public event RoutedEventHandler PencilToolItemClicked;
		private void OnPencilToolItemClick(object sender, RoutedEventArgs e)
		{
			PencilToolItemClicked(sender, e);
		}

		public event RoutedEventHandler ZoomInToolItemClicked;
		private void OnZoomInToolItemClick(object sender, RoutedEventArgs e)
		{
			ZoomInToolItemClicked(sender, e);
		}

		public event RoutedEventHandler HandToolItemClicked;
		private void OnHandToolItemClick(object sender, RoutedEventArgs e)
		{
			HandToolItemClicked(sender, e);
		}

		public event RoutedEventHandler CursorToolItemClicked;
		private void OnCursorToolItemClick(object sender, RoutedEventArgs e)
		{
			CursorToolItemClicked(sender, e);
		}

		public event RoutedEventHandler DeleteItemClicked;
		private void OnDeleteItemClick(object sender, RoutedEventArgs e)
		{
			DeleteItemClicked(sender, e);
		}
	}
}
