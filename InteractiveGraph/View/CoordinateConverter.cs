﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InteractiveGraph.View
{

	class CoordinateConverter : ICoordinateConverter<double>
	{
		private Rect _worldRect;
		private Rect _screenRect;
		private double _scaleX;
		private double _scaleY;
	
		public Rect World
		{
			get => _worldRect;
			set
			{
				_worldRect = value;
				UpdateScale();
			}
		}
		public Rect Screen
		{
			get => _screenRect;
			set
			{
				_screenRect = value;
				UpdateScale();
			}
		}

		public CoordinateConverter()
		{
			World = Screen = new Rect();
		}
		public CoordinateConverter(Rect screen)
		{
			World = Screen = screen;
		}
		public CoordinateConverter(Rect screen, Rect world)
		{
			Screen = screen;
			World = world;
		}

		public IPoint<double> ToWorld(IPoint<double> point)
		{
			if(point == null)
				return null;
			return new PointAdapter(point.X / _scaleX + World.Left,
						            (World.Bottom - point.Y / _scaleY));
		}

		public IPoint<double> ToScreen(IPoint<double> point)
		{
			if(point == null)
				return null;
			return new PointAdapter((point.X - World.Left) * _scaleX,
                                    (World.Bottom - point.Y) * _scaleY);
		}

		private void UpdateScale()
		{
			_scaleX = Screen.Width / World.Width;
			_scaleY = Screen.Height / World.Height;
		}

		public IEnumerable<IPoint<double>> ToWorld(IEnumerable<IPoint<double>> points) =>
			points?.Select(ToWorld);

		public IEnumerable<IPoint<double>> ToScreen(IEnumerable<IPoint<double>> points) =>
			points?.Select(ToScreen);
	}
}
