﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InteractiveGraph.View
{
	interface ICoordinateConverter<T>
	{
		Rect World { get; }
		Rect Screen { get; }

		IPoint<T> ToWorld(IPoint<T> point);
		IPoint<T> ToScreen(IPoint<T> point);

		IEnumerable<IPoint<T>> ToWorld(IEnumerable<IPoint<T>> points);
		IEnumerable<IPoint<T>> ToScreen(IEnumerable<IPoint<T>> points);
	}
}
