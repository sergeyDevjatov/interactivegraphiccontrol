using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InteractiveGraph
{
	class PointAdapter : IPoint<double>
	{
		private Point _point;

		public double X { get => _point.X; set => _point.X = value; }
		public double Y { get => _point.Y; set => _point.Y = value; }

		public PointAdapter(Point point) =>
			_point = point;
		public PointAdapter(double x, double y) =>
			_point = new Point(x, y);

		public Point ToPoint() =>
			_point;
	}
}
