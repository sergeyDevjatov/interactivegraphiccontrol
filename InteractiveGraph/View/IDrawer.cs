﻿using System.Collections.Generic;
using System.Windows.Media;

namespace InteractiveGraph.View
{
	interface IDrawer
	{
		void DrawSelector(ISelector selector, Brush background, Brush border);
		void DrawPoints(IEnumerable<IPoint<double>> points, Brush pointBrush, double radius);
		void DrawLines(IEnumerable<IPoint<double>> points, Brush lineBrush, double thickness);
		void DrawScale(IEnumerable<IPoint<double>> points, Brush lineBrush, double lineThickness);
	}
}
