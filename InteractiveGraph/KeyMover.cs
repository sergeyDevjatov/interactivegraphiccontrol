﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveGraph
{
	class KeyMover : IMover
	{
		private IGraphViewModel _model;

		static private IPoint<double> Move(IPoint<double> point, IPoint<double> displacement) =>
			new PointAdapter(point.X + displacement.X, point.Y + displacement.Y);

		public KeyMover(IGraphViewModel model)
		{
			_model = model;
		}

		public void Move(IPoint<double> displacement)
		{
			_model.SelectedPoints = new ObservableCollection<IPoint<double>>(_model.SelectedPoints.Select(x => Move(x, displacement)));
		}
	}
}
