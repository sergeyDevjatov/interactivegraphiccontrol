﻿namespace InteractiveGraph
{
	interface IMover
	{
		void Move(IPoint<double> displacement);
	}
}
