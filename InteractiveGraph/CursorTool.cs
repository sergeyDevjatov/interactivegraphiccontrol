﻿using InteractiveGraph.Utils;
using InteractiveGraph.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveGraph
{
	class CursorTool : ITool
	{
		IGraphViewModel _model;
		ICoordinateConverter<double> _converter;
		public CursorTool(IGraphViewModel model)
		{
			_model = model;
		}

		private void OnMouseButtonDown(IPoint<double> position)
		{
			IPoint<double> aimedPoint = _model.HoverPoints.FirstOrDefault(x => IsMouseOnPoint(position, x, pointRadius));
			if(_model.SelectedPoints.Any(x => IsMouseOnPoint(e, x)))
			{
				_mover = new MouseMover(_model, _converter.ToWorld(new PointAdapter(e.GetPosition(this))));
			}
			else if(aimedPoint != null)
			{
				RefreshPoints();
				_model.Points.Remove(aimedPoint);
				_model.SelectedPoints.Add(aimedPoint);
				_mover = new MouseMover(_model, _converter.ToWorld(new PointAdapter(e.GetPosition(this))));
			}
			else
			{
				RefreshPoints();
				_selector = new Selector(_converter.ToWorld(new PointAdapter(e.GetPosition(this))), _model);
			}
		}

		public void OnLeftMouseButtonDown(IPoint<double> position)
		{
			throw new NotImplementedException();
		}

		public void OnLeftMouseButtonMouseUp(IPoint<double> position)
		{
			throw new NotImplementedException();
		}

		public void OnMouseMove(IPoint<double> position)
		{
			throw new NotImplementedException();
		}

		public void OnRightMouseButtonMouseDown(IPoint<double> position)
		{
			throw new NotImplementedException();
		}

		public void OnRightMouseButtonMouseUp(IPoint<double> position)
		{
			throw new NotImplementedException();
		}
	}
}
