﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using InteractiveGraph.Utils;

namespace InteractiveGraph
{
	class Selector : ISelector
	{
		private IPoint<double> _from;
		private IPoint<double> _to;
		private IGraphViewModel _model;

		public Selector(IPoint<double> p, IGraphViewModel model)
		{
			_model = model;
			_model.PropertyChanged += OnModelPropertyChanged;
			From = p;
			To = From;
		}

		private void OnModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "Points")
				Update();
		}

		private void Update()
		{
			_model.HoverPoints = new ObservableCollection<IPoint<double>>(_model.Points.Where(x => Frame.Contains(x.ToPoint())));
		}

		public void Select()
		{
			_model.SelectedPoints = _model.HoverPoints;
			_model.HoverPoints = null;
			_model.Points = new ObservableCollection<IPoint<double>>(_model.Points.Except(_model.SelectedPoints));
		}

		public Rect Frame
		{
			get
			{
				if(From == null || To == null)
					return new Rect();
				return new Rect(From.ToPoint(), To.ToPoint());
			}
		}
		public IPoint<double> From
		{
			get => _from;
			set
			{
				_from = value;
				Update();
			}
		}
		public IPoint<double> To
		{
			get => _to;
			set
			{
				_to = value;
				Update();
			}
		}
	}
}
