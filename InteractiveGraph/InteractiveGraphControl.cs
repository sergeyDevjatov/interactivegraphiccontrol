﻿using InteractiveGraph.View;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using InteractiveGraph.Utils;

namespace InteractiveGraph
{
	/// <summary>
	/// Логика взаимодействия для InteractiveGraphControl.xaml
	/// </summary>

	public partial class InteractiveGraphControl : Control
	{
		private GraphContextMenu _contextMenu;
		private enum Tool { Cursor, Pencil, Hand, ZoomIn, ZoomOut }
		private Tool _tool = Tool.Cursor;
		private ICoordinateConverter<double> _converter = new CoordinateConverter();
		private IGraphViewModel _model = new GraphViewModel();
		private ISelector _selector;
		private IMover _mover;
		private IPoint<double> _lastPoint;
		private double _scaleDisplacement = 0;
		private double ScaleDisplacement { get => _scaleDisplacement; set { _scaleDisplacement = value; InvalidateVisual(); } }
		private double _scaleFactor = 1;
		private double ScaleFactor { get => _scaleFactor; set { _scaleFactor = value; InvalidateVisual(); } }

		public InteractiveGraphControl()
		{
			_model.PropertyChanged += OnModelPropertyChanged;
			ContextMenuInit();
			PointBrush = Brushes.Black;
			HoverPointBrush = Brushes.Orange;
			SelectedPointBrush = Brushes.Green;
			SelectorBackgroundBrush = Brushes.AliceBlue;
			SelectorBorderBrush = Brushes.Blue;
			PointRadius = 3;
		}

		private void ContextMenuInit()
		{
			_contextMenu = new GraphContextMenu();
			_contextMenu.DeleteItemClicked += OnDeleteItem_PointContextMenuClicked;
			_contextMenu.Opened += OnPointContextMenuOpened;
			_contextMenu.PlacementTarget = this;
			_contextMenu.CursorToolItemClicked += (sender, e) => { _tool = Tool.Cursor; Cursor = Cursors.Arrow; };
			_contextMenu.PencilToolItemClicked += (sender, e) => { _tool = Tool.Pencil; Cursor = Cursors.Pen; };
			_contextMenu.ZoomInToolItemClicked += (sender, e) => { _tool = Tool.ZoomIn; Cursor = CursorsUtils.ZoomIn; };
			_contextMenu.ZoomOutToolItemClicked += (sender, e) => { _tool = Tool.ZoomOut; Cursor = CursorsUtils.ZoomOut; };
			_contextMenu.HandToolItemClicked += (sender, e) => { _tool = Tool.Hand; Cursor = Cursors.SizeAll; };
			
			ContextMenu = _contextMenu;
		}

		private void OnPointContextMenuOpened(object sender, RoutedEventArgs e)
		{
			_contextMenu.DeleteItem.IsEnabled = _model.SelectedPoints.Count > 0;

		}

		private void OnDeleteItem_PointContextMenuClicked(object sender, RoutedEventArgs e)
		{
			_model.SelectedPoints = null;
		}

		private bool IsMouseOnPoint(MouseEventArgs e, IPoint<double> checkPoint)
		{
			IPoint<double> screenCheckPoint = _converter.ToScreen(checkPoint);
			IPoint<double> mousePos = new PointAdapter(e.GetPosition(this));
			double deltaX = screenCheckPoint.X - mousePos.X;
			double deltaY = screenCheckPoint.Y - mousePos.Y;
			return (Math.Pow(deltaX, 2) + Math.Pow(deltaY, 2)) < (Math.Pow(PointRadius, 2));
		}

		private void OnModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "Points")
			{
				Points = new ObservableCollection<Point>(_model.Points.Select(x => x.ToPoint()));
				IPoint<double> inScreen = null;
				if(_selector != null)
					inScreen = _converter.ToScreen(_selector.From);
				if(_selector != null)
					_selector.From = _converter.ToWorld(inScreen);
			}
			UpdateConverter();
			InvalidateVisual();
		}

		private bool PointsExist()
		{
			return Points != null && Points.Count() > 0;
		}

		private void UpdateConverter()
		{
			if(!PointsExist())
				return;
			Rect world, screen;
			IPoint<double> min, max;
			var points = AllPoints;
			min = new PointAdapter(points.Min(p => p.X) * ScaleFactor + _scaleDisplacement, points.Min(p => p.Y));
			max = new PointAdapter(points.Max(p => p.X) * ScaleFactor + _scaleDisplacement, points.Max(p => p.Y));
			world = new Rect(min.ToPoint(), max.ToPoint());
			screen = new Rect(0, 0, ActualWidth, ActualHeight);
			_converter = new CoordinateConverter(screen, world);
		}

		private void UpdateSelector(Point mousePos)
		{
			if(_selector == null)
				return;
			_selector.To = _converter.ToWorld(new PointAdapter(mousePos));
		}

		private void RefreshPoints()
		{
			_model.Points = new ObservableCollection<IPoint<double>>(AllPoints);
			_model.HoverPoints = _model.SelectedPoints = null;
		}

		private IEnumerable<IPoint<double>> AllPoints
		{
			get => _model.Points.Concat(_model.HoverPoints).Concat(_model.SelectedPoints);
		}

		protected override void OnRender(DrawingContext dc)
		{
			base.OnRender(dc);
			UpdateConverter();
			dc.DrawRectangle(Background ?? Brushes.Gray, null, new Rect(0, 0, ActualWidth, ActualHeight));
			IDrawer drawer = new Drawer(dc, _converter);
			drawer.DrawScale(AllPoints, Brushes.LightGray, 1);
			drawer.DrawSelector(_selector, SelectorBackgroundBrush, SelectorBorderBrush);
			drawer.DrawLines(AllPoints, Brushes.Blue, 0.5);
			drawer.DrawPoints(Points.Select(x => new PointAdapter(x)), PointBrush, PointRadius);
			drawer.DrawPoints(_model.HoverPoints, HoverPointBrush, PointRadius);
			drawer.DrawPoints(_model.SelectedPoints, SelectedPointBrush, PointRadius);
		}

		#region Properties

		#region Points

		public static readonly DependencyProperty PointsProperty;

		public ObservableCollection<Point> Points
		{
			get => (ObservableCollection<Point>)GetValue(PointsProperty);
			set => SetValue(PointsProperty, value);
		}

		private static void OnPointsPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if(d.GetValue(PointsProperty) is ObservableCollection<Point> points)
			{
				points.CollectionChanged += (sender, eargs)
					=> OnPointsChanged(d as InteractiveGraphControl, eargs);
				points.CollectionChanged += (sender, eargs) 
					=> (d as InteractiveGraphControl)?.InvalidateVisual();
			}
		}

		private static void OnPointsChanged(InteractiveGraphControl sender, NotifyCollectionChangedEventArgs e)
		{
			if(sender == null || e == null)
				return;
			IGraphViewModel model = sender._model;
			foreach(Point item in e.NewItems)
				model.Points.Add(new PointAdapter(item));
		}

		#endregion

		#region PointBrush

		public static readonly DependencyProperty PointBrushProperty;

		public Brush PointBrush
		{
			get => (Brush)GetValue(PointBrushProperty);
			set => SetValue(PointBrushProperty, value);
		}

		private static void InvalidateOnPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			(d as InteractiveGraphControl)?.InvalidateVisual();
		}

		#endregion

		#region HoverPointBrush

		public static readonly DependencyProperty HoverPointBrushProperty;

		public Brush HoverPointBrush
		{
			get => (Brush)GetValue(HoverPointBrushProperty);
			set => SetValue(HoverPointBrushProperty, value);
		}

		#endregion

		#region SelectedPointBrush

		public static readonly DependencyProperty SelectedPointBrushProperty;

		public Brush SelectedPointBrush
		{
			get => (Brush)GetValue(SelectedPointBrushProperty);
			set => SetValue(SelectedPointBrushProperty, value);
		}

		#endregion

		#region SelectorBackgroundBrush

		public static readonly DependencyProperty SelectorBackgroundBrushProperty;

		public Brush SelectorBackgroundBrush
		{
			get => (Brush)GetValue(SelectorBackgroundBrushProperty);
			set => SetValue(SelectorBackgroundBrushProperty, value);
		}

		#endregion

		#region SelectorBorderBrush

		public static readonly DependencyProperty SelectorBorderBrushProperty;

		public Brush SelectorBorderBrush
		{
			get => (Brush)GetValue(SelectorBorderBrushProperty);
			set => SetValue(SelectorBorderBrushProperty, value);
		}

		#endregion

		#region PointRadius

		public static readonly DependencyProperty PointRadiusProperty;

		public double PointRadius
		{
			get => (double)GetValue(PointRadiusProperty);
			set => SetValue(PointRadiusProperty, value);
		}

		private static void OnPointRadiusPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			InvalidateOnPropertyChange(d, e);
		}

		#endregion

		static InteractiveGraphControl()
		{
			PointsProperty = DependencyProperty.Register("Points",
					typeof(ObservableCollection<Point>),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata(new ObservableCollection<Point>())
					{
						PropertyChangedCallback = OnPointsPropertyChange
					});
			PointBrushProperty = DependencyProperty.Register("PointBrush",
					typeof(Brush),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata()
					{
						PropertyChangedCallback = InvalidateOnPropertyChange
					});
			HoverPointBrushProperty = DependencyProperty.Register("HoverPointBrush",
					typeof(Brush),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata()
					{
						PropertyChangedCallback = InvalidateOnPropertyChange
					});
			SelectedPointBrushProperty = DependencyProperty.Register("SelectedPointBrush",
					typeof(Brush),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata()
					{
						PropertyChangedCallback = InvalidateOnPropertyChange
					});
			SelectorBackgroundBrushProperty = DependencyProperty.Register("SelectorBackgroundBrush",
					typeof(Brush),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata()
					{
						PropertyChangedCallback = InvalidateOnPropertyChange
					});
			SelectorBorderBrushProperty = DependencyProperty.Register("SelectorBorderBrush",
					typeof(Brush),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata()
					{
						PropertyChangedCallback = InvalidateOnPropertyChange
					});
			PointRadiusProperty = DependencyProperty.Register("PointRadius",
					typeof(double),
					typeof(InteractiveGraphControl), new FrameworkPropertyMetadata()
					{
						PropertyChangedCallback = OnPointRadiusPropertyChange
					});
		}

		#endregion

	}
}
