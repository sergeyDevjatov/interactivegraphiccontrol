﻿namespace InteractiveGraph
{
	interface ISelector
	{
		System.Windows.Rect Frame { get; }
		IPoint<double> From { get; set; }
		IPoint<double> To { get; set; }

		void Select();
	}
}
