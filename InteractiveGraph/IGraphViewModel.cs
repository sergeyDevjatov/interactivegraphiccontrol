﻿namespace InteractiveGraph
{
	interface IGraphViewModel : System.ComponentModel.INotifyPropertyChanged
	{
		System.Collections.ObjectModel.ObservableCollection<IPoint<double>> Points { get; set; }
		System.Collections.ObjectModel.ObservableCollection<IPoint<double>> HoverPoints { get; set; }
		System.Collections.ObjectModel.ObservableCollection<IPoint<double>> SelectedPoints { get; set; }
	}
}
