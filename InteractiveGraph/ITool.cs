﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveGraph
{
	interface ITool
	{
		void OnMouseMove(IPoint<double> position);
		void OnLeftMouseButtonDown(IPoint<double> position);
		void OnLeftMouseButtonMouseUp(IPoint<double> position);
		void OnRightMouseButtonMouseDown(IPoint<double> position);
		void OnRightMouseButtonMouseUp(IPoint<double> position);
	}
}
