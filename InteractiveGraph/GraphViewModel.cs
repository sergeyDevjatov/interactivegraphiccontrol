﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InteractiveGraph
{
	class GraphViewModel : IGraphViewModel
	{
		private ObservableCollection<IPoint<double>> _points;
		public ObservableCollection<IPoint<double>> Points
		{
			get => _points;
			set
			{
				_points = value ?? new ObservableCollection<IPoint<double>>();
				_points.CollectionChanged += (sender, e) => OnPropertyChanged("Points");
				OnPropertyChanged("Points");
			}
		}

		private ObservableCollection<IPoint<double>> _hoverPoints;
		public ObservableCollection<IPoint<double>> HoverPoints
		{
			get => _hoverPoints;
			set
			{
				_hoverPoints = value ?? new ObservableCollection<IPoint<double>>();
				HoverPoints.CollectionChanged += (sender, e) => OnPropertyChanged("HoverPoints");
				OnPropertyChanged("HoverPoints");
			}
		}

		private ObservableCollection<IPoint<double>> _selectedPoints;
		public ObservableCollection<IPoint<double>> SelectedPoints
		{
			get => _selectedPoints;
			set
			{
				_selectedPoints = value ?? new ObservableCollection<IPoint<double>>();
				SelectedPoints.CollectionChanged += (sender, e) => OnPropertyChanged("SelectedPoints");
				OnPropertyChanged("SelectedPoints");
			}
		}

		public GraphViewModel()
		{
			Points = new ObservableCollection<IPoint<double>>();
			HoverPoints = new ObservableCollection<IPoint<double>>();
			SelectedPoints = new ObservableCollection<IPoint<double>>();
		}

		#region INPC-related code

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion
	}
}
