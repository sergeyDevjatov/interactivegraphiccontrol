﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphTest
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		Random rand = new Random();

		public MainWindow()
		{
			InitializeComponent();
			SetRandPoint();
			DataContext = new ViewModel();
		}

		private void SetRandPoint()
		{
			xTextbox.Text = rand.Next(-150, 150).ToString();
			yTextbox.Text = rand.Next(-100, 100).ToString();
		}

		private void Add(object sender, RoutedEventArgs e)
		{
			var viewModel = DataContext as ViewModel;
			viewModel.Add();
			SetRandPoint();
		}
	}
}
