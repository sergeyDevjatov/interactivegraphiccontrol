﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphTest
{
	class Model
	{
		public SortedList<double, double> Points { get; set; } = new SortedList<double, double>();

		public void Add(Point point) => Points.Add(point.X, point.Y);
		public void AddRange(Point[] points)
		{
			foreach(var p in points)
				Add(p);
		}
		public void Remove(Point point) => Points.Remove(point.X);
		public void RemoveRange(Point[] points)
		{
			foreach(var p in points)
				Remove(p);
		}
	}
}
