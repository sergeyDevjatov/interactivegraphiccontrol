﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphTest
{
	class ViewModel : INotifyPropertyChanged
	{
		public class StrPoint
		{
			public string X { get; set; } = "";
			public string Y { get; set; } = "";

			public static explicit operator Point(StrPoint strPoint)
			{
				if(double.TryParse(strPoint.X, out double x) && double.TryParse(strPoint.Y, out double y))
					return new Point(x, y);
				return new Point();
			}
		}
		
		private ObservableCollection<Point> _points;
		public ObservableCollection<Point> Points
		{
			get { return _points; }
			set
			{
				_points = value;
				OnPropertyChanged("Points");
			}
		}

		private StrPoint _selectedPoint = new StrPoint();
		public StrPoint SelectedPoint
		{
			get { return _selectedPoint; }
			set
			{
				_selectedPoint = value;
				OnPropertyChanged("SelectedPoint");
			}
		}

		private Model _model = new Model();

		public ViewModel()
		{
			Points = new ObservableCollection<Point>(_model.Points.Select(p => new Point(p.Key, p.Value)));
		}

		public void Add() => Points.Add((Point)SelectedPoint);

		public void Remove() => Points.Remove((Point)SelectedPoint);


		#region INPC-related code

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName) =>
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		#endregion
	}
}
